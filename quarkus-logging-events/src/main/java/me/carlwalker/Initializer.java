package me.carlwalker;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import org.jboss.logging.Logger;

@ApplicationScoped
public class Initializer {

    @Inject
    Logger LOGGER;

    public void onInit(@Observes StartupEvent startupEvent) {
        LOGGER.info("the app has started up");
    }

    public void onDestroy(@Observes ShutdownEvent shutdownEvent) {
        LOGGER.info("the app is shutting down");
    }
}
