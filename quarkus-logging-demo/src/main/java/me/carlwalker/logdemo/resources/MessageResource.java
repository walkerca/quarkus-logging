package me.carlwalker.logdemo.resources;

import org.jboss.logging.Logger;
import services.MessageService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/message")
public class MessageResource {

    @Inject
    Logger LOGGER;

    @Inject
    MessageService messageService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessage() {
        LOGGER.trace("getMessage() was called");
        return Response
                .ok()
                .entity(
                    messageService.getMessage()
                )
                .build();
    }
}
