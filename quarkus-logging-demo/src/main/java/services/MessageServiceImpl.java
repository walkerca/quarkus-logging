package services;

import me.carlwalker.logdemo.resources.Message;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MessageServiceImpl implements MessageService {

    @Inject
    Logger LOGGER;

    @Inject
    MessageConfiguration messageConfiguration;

    @Override
    public Message getMessage() {
        var m = messageConfiguration.message();
        LOGGER.trace("getMessage() called");
        LOGGER.debug("found '" + m + "' in yaml");
        return new Message(m);
    }
}
