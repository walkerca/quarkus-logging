package services;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix="m",namingStrategy = ConfigMapping.NamingStrategy.VERBATIM)
public interface MessageConfiguration {
    String message();
}
