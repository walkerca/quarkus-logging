package services;

import me.carlwalker.logdemo.resources.Message;

public interface MessageService {

    Message getMessage();
}
